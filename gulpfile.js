var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');


//SVG
var svgmin = require('gulp-svgmin');
var rename = require('gulp-rename');
var svgstore = require('gulp-svgstore');
var cheerio = require('gulp-cheerio');
var inject = require('gulp-inject');
var path            = require('path');

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        proxy: "localhost:5000"
    });

    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch("public/*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.stream());
});



/*
 * SVG Sprite
 */



gulp.task('svgsprite', function() {

    var svgs = gulp.src('./svgs/*.svg')
        .pipe(rename({prefix: 'icon-'}))
        .pipe(svgmin({
            plugins: [
                {removeStyleElement: true},
                {removeViewBox: true},
                {removeUselessStrokeAndFill: true}
            ]
        }))
        .pipe(svgstore({
            inlineSvg: false
        }))
        .pipe(rename('sprite.svg')); // must be run after svgstore pipe
    return gulp
        .src('./svgs/html/src/index.html')

        .pipe(inject(
            svgs,
            {
                starttag: '<!-- inject:source -->',
                transform: function(filePath, file) {
                    return file.contents.toString();
                }
            }
        ))
        .pipe(inject(
            gulp.src('./svgs/*.svg', {read: false}), {
                transform: function (filepath) {
                    var name = path.basename(filepath,'.svg');
                    /*
                     * Uncomment line bellow to see the svgs
                     */
                    return '<svg class="icon"><use xlink:href="#icon-'+ name + '" /></svg>';
                }
            }
        ))
        .pipe(gulp.dest('./svgs/html/dist'));

});

gulp.task('default', ['serve']);