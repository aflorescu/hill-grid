#Responsive table grid demo


##Run setup locally

Make sure you have node installed.
Run `npm install` - with `sudo` on mac
Run `npm run local` - this will open two ports: 5000 and 3000, the later being a proxy to alow CSS injections and file watch reload.
 
##Online demo: 

Link: https://hill-grid.firebaseapp.com/
 
##About
The project it's built with Angular for interactivity and Firebase for the data.


##Design
The desktop design is very different from the mobile design, but it essentially maintains the same data.
Choosing the right design depends on a lot of factors, but this design maintains it's functionality and does not create clutter or requires the user to scroll/swipe horizontally for the information. 

 
##Technical details

Used Firebase CLI for easy deployment in the cloud.


Angular Fire is used to fetch the data from the clout, just to see how the project works with some data fetched remotely.


NPM stores some modules in package.json used in this project for faster developement - gulp, browsersync for now.


A script in the package.json files allows me to run two separate tasks one after the other - in this case, the firebase server and the browsersync server.
The firebase server deals with serving the public directory and the browsersync for injecting the CSS and reloading when needed.


The CSS is preprocessed and build with Gulp and written in SCSS syntax.

The breakpoints are defined on the spot, based on the current layout with help from the http://breakpoint-sass.com/ project


On the JavaScript side, only one controller written that fetches the data and handles a click event. 
 
